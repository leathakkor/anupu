﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace Anupu.Web.Authentication
{
    internal class AnupuSettingsWrapper : IAnupuSettings
    {
        private readonly AnupuSettings settings;
        
        private readonly Configuration config;
        private readonly Configuration _config;
        
        public AnupuSettingsWrapper(AnupuSettings settings, Configuration config)
        {
            this.settings = settings;
            this.config = config;
            this._config = config;
        }
        
        public bool HasUsers
        {
            get{ return this.settings.Users.Count > 0; }
        }
        
        public bool HasExclusions
        {
            get { return this.settings.Exclusions.Count > 0; }
        }
        
        IEnumerable<IAnupuUser> IAnupuSettings.Users
        {
            get
            {
                return this.settings.Users.CreateCopy();
            }
        }
        
        IEnumerable<IExclusion> IAnupuSettings.Exclusions
        {
            get
            {
                return this.settings.Exclusions.CreateCopy();
            }
        }
        
        
        /// <summary>
        /// The Realm associated with this Instance.
        /// </summary>
        public string ZoneName
        {
            get { return this.settings.ZoneName; }
            set { this.settings.ZoneName = value; }
        }
        
        /// <summary>
        /// This property is used to determine if the backend data stre is marked as readonly.
        /// </summary>
        public bool IsInMemory
        {
            get { return this.settings.IsInMemory; }
            set { this.settings.IsInMemory = value; }
        }
        
        /// <summary>
        /// The text to display when a user is locked out of the site.
        /// </summary>
        public string LockedOutText
        {
            get { return this.settings.LockedOutText; }
        }
        
        /// <summary>
        /// The text to display when a user is locked out of the site.
        /// </summary>
        public string AuthenticationInstructions
        {
            get { return this.settings.AuthenticationInstructions; }
        }
        
        /// <summary>
        /// Saves the configuration to the config file.
        /// </summary>
        public void Save()
        {
            if(!IsInMemory)
            {
                _config.Save();
            }
        }
        
        void IAnupuSettings.AddUser(string userName, string password, int maxInvalidAttempts)
        {
            this.PerformDangerousOp((x) => settings.Users.Add(userName, password, maxInvalidAttempts));
        }
        
        void IAnupuSettings.RemoveUser(string userName)
        {
            this.PerformDangerousOp((x) => settings.Users.Remove(userName));
        }
        
        public IAnupuUser FindUserByName(string userName)
        {
            return this.settings.Users.FindByKey(userName);
        }
        
        public IExclusion FindExclusionByDomain(string domain)
        {
            return this.settings.Exclusions.FindByDomain(domain);
        }
        
        public void AddExclusion(string key, string domain)
        {
            this.PerformDangerousOp((x) =>
                                    {
                                        if (this.settings.Exclusions.FindByKey(key) != null)
                                        {
                                            throw new ArgumentException("Duplicate key: '" + key + "'");
                                        }
                                        
                                        this.settings.Exclusions.Add(key, domain);
                                    });
        }
    }
}

//
//
//        AnupuReason IAnupuSettings.AuthenticateUser(string username, string pasword)
//        {
//            return this.DoAuthenticate(username, pasword);
//        }