﻿/* Created By tyoung On 1/16/2010 11:14 PM */
using System;
using System.Configuration;


namespace Anupu.Web.Authentication
{
	/// <summary>
	/// The Collection Of Users.
	/// </summary>
	internal interface IUsersCollection : IConfigurationCollection<IAnupuUser>
	{
	}
}

