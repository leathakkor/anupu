﻿/* Created By tyoung On 1/17/2010 1:51 AM */
using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;

namespace Anupu.Web.Authentication
{
	/// <summary>
	///This class contains much of the business logic for authenticating
	/// </summary>
	public sealed class WebAuthenticationProcessor
	{
	    private readonly AuthenticationProcessor processor;
	    
	    /// <summary>
	    /// Create an Instance
	    /// </summary>
	    /// <param name="sectionName"></param>
	    /// <param name="mapper"></param>
	    public WebAuthenticationProcessor(String sectionName, IPathMapper mapper)
	    {
	        this.processor = new AuthenticationProcessor(sectionName, mapper);
	    }
	    
	    /// <summary>
	    /// The Collection of users.
	    /// </summary>
	    public IAnupuSettings Settings
	    {
	        get { return this.processor.Settings; }
	    }
	    
	    /// <summary>
	    /// This method processes the Http context as a module.
	    /// </summary>
	    /// <param name="base">The request being processed.</param>
	    public void ProcessRequest(HttpContextBase @base)
	    {
	        var reason = this.processor.Process(@base.Request.Headers, @base.Response.Headers, @base.Request.Url);
	        if(AnupuReason.LockedOut == reason)
	        {
	            @base.Response.Clear();
	            var lockedOutText = "You are locked out due to invalid password attempts. Please contact an adminstrator.";
	            if(!String.IsNullOrEmpty(this.Settings.LockedOutText))
	            {
	                lockedOutText = this.Settings.LockedOutText;
	            }
	            
	            @base.Response.Write(lockedOutText);
	            @base.Response.StatusCode = 401;
	            @base.Response.End();
	        }
	        else if(AnupuReason.Success != reason)
	        {
	            @base.Response.Clear();
	            var authenticationInstructions = "Please Authenticate";
	            if(!String.IsNullOrEmpty(this.Settings.AuthenticationInstructions))
	            {
	                authenticationInstructions = this.Settings.AuthenticationInstructions;
	            }
	            
	            @base.Response.Write(authenticationInstructions);
	            @base.Response.StatusCode = 401;
	            @base.Response.Headers.Add("WWW-Authenticate", String.Format(@"Basic realm=""{0}""", this.Settings.ZoneName));
	            @base.Response.End();
	        }
	    }
	}
}
