﻿/* Created By tyoung On 1/16/2010 11:14 PM */
using System;
using System.Configuration;


namespace Anupu.Web.Authentication
{
    /// <summary>
    /// This class is a wrapping base class for the collection.
    /// </summary>
    public abstract class AnupuConfigurationCollectionBase<T, TReturnType>
        : ConfigurationElementCollection, IConfigurationCollection<TReturnType>
        where T : ConfigurationElement, TReturnType, new()
    {   
        /// <summary>
        /// Gets the CollectionType of the ConfigurationElementCollection.
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }
        
        /// <summary>
        /// This method Finds an Element By its ordinal position.
        /// </summary>
        /// <param name="index">The index of the item to find.</param>
        /// <returns>The Found Item.</returns>
        public TReturnType FindByIndex(int index)
        {
            return this.Get(index);
        }
        
        /// <summary>
        /// This method returns the item by key.
        /// </summary>
        /// <param name="key">The key used to find the item.</param>
        /// <returns>The Item found.</returns>
        public TReturnType FindByKey(string key)
        {
            return this.Get(key);
        }
        
        System.Collections.Generic.IEnumerator<TReturnType> System.Collections.Generic.IEnumerable<TReturnType>.GetEnumerator()
        {
            foreach (var element in this)
            {
                yield return (TReturnType)element;
            }
        }
        
        
        /// <summary>
        /// Adds a UsersElement to the configuration file.
        /// </summary>
        /// <param name="element">The UsersElement to add.</param>
        internal void Add(T element)
        {
            BaseAdd(element);
        }
        
        /// <summary>
        /// Removes a UsersElement with the given name.
        /// </summary>
        /// <param name="name">The name of the UsersElement to remove.</param>
        internal void Remove(string name)
        {
            base.BaseRemove(name);
        }
        
        internal T Get(string key)
        {
            return (T)BaseGet(key.ToLowerInvariant());
        }
        
        
        internal T Get(int index)
        {
            return (T)BaseGet(index);
        }
        
        
        /// <summary>
        /// Creates a new UsersElement.
        /// </summary>
        /// <returns>A new <c>UsersElement</c></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new T();
        }
        
        /// <summary>
        /// Gets the key of an element based on it's Id.
        /// </summary>
        /// <param name="element">Element to get the key of.</param>
        /// <returns>The key of <c>element</c>.</returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return this.GetKey(((T)element));
        }
        
        /// <summary>
        /// Gets the element's key.
        /// </summary>
        /// <param name="item">The item for which the key will be gotten.</param>
        /// <returns>The Found key.</returns>
        protected abstract string GetKey(T item);
        
    }
}

