﻿/* Created By tyoung On 1/16/2010 11:15 PM */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Configuration;

namespace Anupu.Web.Authentication
{
    /// <summary>
    /// Configuration section &lt;Users&gt;
    /// </summary>
    /// <remarks>
    /// Assign properties to your child class that has the attribute
    /// <c>[ConfigurationProperty]</c> to store said properties in the xml.
    /// </remarks>
    internal class AnupuSettings : ConfigurationSection//, IAnupuSettings
    {
        protected internal AnupuSettings()
        {
        }
        
        /// <summary>
        /// A custom XML section for an application's configuration file.
        /// </summary>
        [ConfigurationProperty("users", IsDefaultCollection = true)]
        public UsersCollection Users
        {
            get { return (UsersCollection)base["users"]; }
        }
        
        /// <summary>
        /// A custom XML section for an application's configuration file.
        /// </summary>
        [ConfigurationProperty("exclusions", IsDefaultCollection = false)]
        public ExclusionsCollection Exclusions
        {
            get { return (ExclusionsCollection)base["exclusions"]; }
        }
        
        /// <summary>
        /// A custom XML section for an application's configuration file.
        /// </summary>
        [ConfigurationProperty("zone-name", IsRequired = true)]
        public String ZoneName
        {
            get { return (string)base["zone-name"]; }
            set { base["zone-name"] = value; }
        }
        
        /// <summary>
        /// A custom XML section for an application's configuration file.
        /// </summary>
        [ConfigurationProperty("account-locked-out-text", IsRequired = false)]
        public String LockedOutText
        {
            get { return (string)base["account-locked-out-text"]; }
            set { base["account-locked-out-text"] = value; }
        }
        
        /// <summary>
        /// A custom XML section for an application's configuration file.
        /// </summary>
        [ConfigurationProperty("authentication-instructions", IsRequired = false)]
        public String AuthenticationInstructions
        {
            get { return (string)base["authentication-instructions"]; }
            set { base["authentication-instructions"] = value; }
        }
        
        /// <summary>
        /// A custom XML section for an application's configuration file.
        /// </summary>
        [ConfigurationProperty("is-read-only", IsRequired = true, DefaultValue=false)]
        public bool IsInMemory
        {
            get { return (bool)base["is-read-only"]; }
            set { base["is-read-only"] = value; }
        }
        
        /// <summary>
        /// A custom XML section for an application's configuration file.
        /// </summary>
        [ConfigurationProperty("user-lockout-duration", IsRequired = true)]
        public TimeSpan LockDuration
        {
            get { return (TimeSpan)base["user-lockout-duration"]; }
            set { base["user-lockout-duration"] = value; }
        }
    }
}



//        IAnupuUser IAnupuSettings.FindUserByName(string userName)
//        {
//            return this.Users.Get(userName);
//        }