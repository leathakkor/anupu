﻿/* Created By tyoung On 1/16/2010 11:14 PM */
using System;
using System.Configuration;

namespace Anupu.Web.Authentication
{
	/// <summary>
	/// Represents a single XML tag inside a ConfigurationSection
	/// or a ConfigurationElementCollection.
	/// </summary>
	public sealed class ExclusionElement : ConfigurationElement, IExclusion
	{
		/// <summary>
		/// The attribute <c>key</c> of a <c>ExclusionElement</c>.
		/// </summary>
		[ConfigurationProperty("key", IsKey = true, IsRequired = true)]
		public string Key {
			get { return (string)this["key"]; }
			set { this["key"] = value.ToLowerInvariant(); }
		}
	
		/// <summary>
		/// The attribute <c>domain</c> of a <c>ExclusionElement</c>.
		/// </summary>
		[ConfigurationProperty("domain", IsRequired = true)]
		public string Domain {
			get { return (string)this["domain"]; }
			set { this["domain"] = value; }
		}
	}
}

