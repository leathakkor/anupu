﻿/* Created By tyoung On 1/16/2010 11:14 PM */
using System;
using System.Configuration;

namespace Anupu.Web.Authentication
{
	/// <summary>
	/// Represents a single XML tag inside a ConfigurationSection
	/// or a ConfigurationElementCollection.
	/// </summary>
	public sealed class UsersElement : ConfigurationElement, IAnupuUser
	{
		/// <summary>
		/// The attribute <c>name</c> of a <c>UsersElement</c>.
		/// </summary>
		[ConfigurationProperty("username", IsKey = true, IsRequired = true)]
		public string Name {
			get { return (string)this["username"]; }
			set { this["username"] = value.ToLowerInvariant(); }
		}

		/// <summary>
		/// The attribute <c>name</c> of a <c>UsersElement</c>.
		/// </summary>
		[ConfigurationProperty("password", IsRequired = true)]
		public string Password {
			get { return (string)this["password"]; }
			set { this["password"] = value; }
		}


		/// <summary>
		/// The attribute <c>name</c> of a <c>UsersElement</c>.
		/// </summary>
		[ConfigurationProperty("max-invalid-attempts", IsRequired = true)]
		public int MaxInvalidAttempts {
			get { return (int)this["max-invalid-attempts"]; }
			set { this["max-invalid-attempts"] = value; }
		}


		/// <summary>
		/// The attribute <c>name</c> of a <c>UsersElement</c>.
		/// </summary>
		[ConfigurationProperty("current-invalid-attempts", IsRequired = true)]
		public int CurrentInvalidAttempts {
			get { return (int)this["current-invalid-attempts"]; }
			set { this["current-invalid-attempts"] = value; }
		}
	}
	

}

