﻿
namespace Anupu.Web.Authentication
{
    using System;
    using System.Linq;

    /// <summary>
    /// This class is a set of utility methods for operating on user Settings.
    /// </summary>
    public static class UserSettingsUtilities
    {
        /// <summary>
        /// This method will ensure that there are no active users in Anupu.
        /// </summary>
        /// <param name="settings">The Settings to remove all users from.</param>
        public static void Deactivate(this IAnupuSettings settings)
        {
            while(settings.HasUsers)
            {
                settings.RemoveUser(settings.Users.First().Name);
            }
        }
        
        /// <summary>
	    /// This method throws an Invalid Operation Excpetion if the 
	    /// container is read-only.
	    /// </summary>
	    public static void ThrowExceptionOnIsReadonly(this IAnupuSettings settings)
	    {
	        if(settings.IsInMemory)
	        {
	            throw new InvalidOperationException("You are not allow to update the UserList, when you are using IsInMemory='true'");
	        }
	    }
	    
	    /// <summary>
	    /// This method executes a method then does validation
	    /// and saves the settings.
	    /// </summary>
	    /// <param name="settings">The item on which to perform the action.</param>
	    /// <param name="action">The Action to perform.</param>
	    public static void PerformDangerousOp(this IAnupuSettings settings, Action<IAnupuSettings> action)
	    {
	        action(settings);
	        settings.ThrowExceptionOnIsReadonly();
	        settings.Save();
	    }
	    
	    /// <summary>
	    /// This method performs the actual authentication.
	    /// </summary>
	    /// <param name="settings">The settings used to authenticate.</param>
	    /// <param name="username">The username to authenticate against.</param>
	    /// <param name="password">The password used to authenticate.</param>
	    /// <returns></returns>
	    public static AnupuReason AuthenticateUser(this IAnupuSettings settings, string username, string password)
	    {
	        if (!settings.HasUsers)
	        {
	            return AnupuReason.Success;
	        }
	        
	        //HACK
	        UsersElement user = (UsersElement)settings.FindUserByName(username);
	        
	        if (user == null)
	        {
	            return AnupuReason.UnknownUser;
	        }
	        
	        if (0 < user.MaxInvalidAttempts && user.MaxInvalidAttempts <= user.CurrentInvalidAttempts)
	        {
	            return AnupuReason.LockedOut;
	        }
	        
	        
	        if (string.Equals(user.Password, password, StringComparison.InvariantCultureIgnoreCase))
	        {
	            if (user.CurrentInvalidAttempts != 0)
	            {
	                user.CurrentInvalidAttempts = 0;
	                settings.Save();
	            }
	            
	            return AnupuReason.Success;
	        }
	        
	        
	        user.CurrentInvalidAttempts = user.CurrentInvalidAttempts + 1;
	        settings.Save();
	        return AnupuReason.BadPassword;
	    }
    }
}
