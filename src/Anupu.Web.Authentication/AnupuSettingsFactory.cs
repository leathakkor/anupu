﻿
using System;
using System.Configuration;
using System.IO;

namespace Anupu.Web.Authentication
{
    /// <summary>
    /// Description of AunupuSettingsFactory.
    /// </summary>
    public static class AnupuSettingsFactory
    {
        internal static IAnupuSettings GetSection(String sectionName, IPathMapper mapper)
        {
            // Get the application path.
            string exePath = "~/bin/Anupu.Web.Authentication.dll";
            exePath = mapper.MapPath(exePath);
            try
            {
                if(File.Exists(exePath))
                {
                    var config = ConfigurationManager.OpenExeConfiguration(exePath);
                    return GetSection(config, sectionName);
                }
                
                throw new ConfigurationErrorsException(exePath);
            }
            catch(Exception e)
            {
                throw new ConfigurationErrorsException(exePath, e);
            }
        }
        
        internal static IAnupuSettings GetSection(Configuration config, String sectionName)
        {
            AnupuSettings usersSettings = (AnupuSettings)config.GetSection(sectionName);
            if (usersSettings == null)
            {
                usersSettings = new AnupuSettings();
                config.Sections.Add(sectionName, usersSettings);
                if(!usersSettings.IsInMemory)
                {
                    config.Save();
                }
            }
            
            return new AnupuSettingsWrapper(usersSettings, config);
            
            //            return usersSettings;
        }
        
        
    }

}
