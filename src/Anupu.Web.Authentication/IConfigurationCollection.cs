﻿/* Created By tyoung On 1/16/2010 11:14 PM */
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Anupu.Web.Authentication
{
    /// <summary>
    /// A class that allows you to access items from a configuration collection.
    /// </summary>
    public interface IConfigurationCollection<T> : IEnumerable<T>
    {
        /// <summary>
        /// Gets the number of items in the collection.
        /// </summary>
        int Count { get; }
        
        /// <summary>
        /// This method returns the item by index.
        /// </summary>
        /// <param name="index">The index used to find the item.</param>
        /// <returns>The Item found.</returns>
        T FindByIndex(int index);
        
        /// <summary>
        /// This method returns the item by key.
        /// </summary>
        /// <param name="key">The key used to find the item.</param>
        /// <returns>The Item found.</returns>
        T FindByKey(string key);
    }
}

