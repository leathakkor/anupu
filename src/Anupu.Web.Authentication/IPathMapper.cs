﻿/* Created By tyoung On 1/19/2010 5:57 AM */
using System;

namespace Anupu.Web.Authentication
{
	/// <summary>
	/// This Contract ensures that you are able to map an AppRelative Path
	/// 	To an absolute Path
	/// </summary>
	public interface IPathMapper
	{
		/// <summary>
		/// Map an AppRelativePath to an Absolute Path
		/// </summary>
		/// <param name="pathToMap"></param>
		/// <returns></returns>
		String MapPath(string pathToMap);
	}
}
