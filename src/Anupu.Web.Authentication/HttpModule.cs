﻿/* Created By tyoung On 1/16/2010 10:57 PM */
using System;
using System.Reflection;
using System.Web;

namespace Anupu.Web.Authentication
{
	/// <summary>
	/// Description of HttpModule.
	/// </summary>
	public class HttpModule : IHttpModule
	{
		private static WebAuthenticationProcessor _processor;
		
		/// <summary>
		/// The Processor Instance
		/// </summary>
		public static WebAuthenticationProcessor Processor
		{
			get { return _processor; }
		}
		
		void System.Web.IHttpModule.Init(System.Web.HttpApplication application)
		{
			var wrapper = new ServerMapPathWrapper(new HttpContextWrapper(application.Context));
			HttpModuleCollection modules;
			try
			{
				modules = application.Modules;
				foreach (var key in modules.AllKeys)
				{
					var module = application.Modules.Get(key);
					if(module == this)
					{
						_processor = new WebAuthenticationProcessor(key, wrapper);
						return;
					}
				}
			}
			catch{}
			if(_processor == null)
			{
				_processor = new WebAuthenticationProcessor("Anupu", wrapper);
			}
			
			
			application.BeginRequest+= delegate(object sender, EventArgs e) {
				var app = sender as HttpApplication;
				_processor.ProcessRequest(new HttpContextWrapper(app.Context));
			};
		}
		
		
		void System.Web.IHttpModule.Dispose()
		{
		}
	}
}
