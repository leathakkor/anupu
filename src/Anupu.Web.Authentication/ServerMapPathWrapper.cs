﻿/* Created By tyoung On 1/19/2010 5:57 AM */
using System;
using System.Web;

namespace Anupu.Web.Authentication
{
	/// <summary>
	/// Description of IPathMapper.
	/// </summary>
	internal class ServerMapPathWrapper : IPathMapper
	{
		private readonly HttpContextBase _base;
		public ServerMapPathWrapper(HttpContextBase @base)
		{
			_base = @base;
		}
		
		string IPathMapper.MapPath(string pathToMap)
		{
			return _base.Server.MapPath(pathToMap);
		}
	}
}
