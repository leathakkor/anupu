﻿/* Created By tyoung On 1/17/2010 12:31 AM */
using System;

namespace Anupu.Web.Authentication
{
	/// <summary>
	/// The Factory for creating an new instance of
	/// 	an <see cref="IAnupuSettings" />
	/// </summary>
	public static class Anupu
	{
		/// <summary>
		/// The Factory method for creating an new instance of
		/// 	an <see cref="IAnupuSettings" />
		/// </summary>
		/// <param name="sectionName">
		/// The ConfigSection Name to Use 
		/// 	when creating or reading the Web.ConfigSection.
		/// </param>
		/// <param name="mapper">
		/// 	An Instance of an <see cref="IPathMapper"/>
		/// </param>
		/// <returns>
		/// AnInstance of <see cref="IAnupuSettings" />
		/// </returns>
		public static IAnupuSettings CreateUserSettings(String sectionName, IPathMapper mapper)
		{
			if(mapper == null)
			{
				throw new ArgumentNullException("mapper");
			}
			
			return AnupuSettingsFactory.GetSection(sectionName, mapper);
		}
	}
}
