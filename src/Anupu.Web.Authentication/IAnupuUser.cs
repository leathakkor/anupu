﻿/* Created By tyoung On 1/16/2010 11:14 PM */
using System;
using System.Configuration;

namespace Anupu.Web.Authentication
{
	/// <summary>
	/// The contract that represents information about a user.
	/// </summary>
	public interface IAnupuUser
	{
		/// <summary>
		/// The User's Name, which is a unique Identifier
		/// </summary>
		string Name { get; }
		
		/// <summary>
		/// The User's Password
		/// </summary>
		string Password { get; }
		
		/// <summary>
		/// The Maximum number of invalid attempts allowed with this user.
		/// </summary>
		int MaxInvalidAttempts { get; }
		
		/// <summary>
		/// The current Number of invalid attempts
		/// </summary>
		int CurrentInvalidAttempts { get; }
	}
}

