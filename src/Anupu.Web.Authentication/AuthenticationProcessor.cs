﻿/* Created By tyoung On 1/17/2010 1:51 AM */
using System;
using System.Collections.Specialized;
using System.Text;
using System.Web;

namespace Anupu.Web.Authentication
{
    /// <summary>
    ///This class contains much of the business logic for authenticating
    /// </summary>
    public sealed class AuthenticationProcessor
    {
        private readonly IAnupuSettings settings;
        
	    /// <summary>
	    /// Create an Instance
	    /// </summary>
	    /// <param name="sectionName"></param>
	    /// <param name="mapper"></param>
	    public AuthenticationProcessor(String sectionName, IPathMapper mapper)
	    {
	        this.settings = Anupu.CreateUserSettings(sectionName, mapper);
	    }
        
        /// <summary>
        /// Create an Instance
        /// </summary>
        /// <param name="settings">The Settings.</param>
        public AuthenticationProcessor(IAnupuSettings settings)
        {
            this.settings = settings;
        }
        
        /// <summary>
        /// The Collection of users.
        /// </summary>
        public IAnupuSettings Settings
        {
            get { return this.settings; }
        }
        
        
        /// <summary>
        /// This is a utility methor that converts a string into
        ///		UT8 Encoded Base-64.
        /// </summary>
        /// <param name="data">The data to encode.</param>
        /// <returns>The encoded data.</returns>
        public string Base64Decode(string data)
        {
            byte[] todecode_byte = Convert.FromBase64String(data);
            return Encoding.UTF8.GetString(todecode_byte);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputHeaders"></param>
        /// <param name="outputHeaders"></param>
        /// <param name="uri"></param>
        /// <returns>
        /// The Status Code
        /// 	Return 0 if you do not wish to set a code.
        ///</returns>
        public AnupuReason Process(NameValueCollection inputHeaders, NameValueCollection outputHeaders, Uri uri)
        {
            if (this.settings.HasExclusions)
            {
                if (this.settings.FindExclusionByDomain(uri.Host.ToLowerInvariant()) != null)
                {
                    return AnupuReason.Success;
                }
            }
            
            if(this.settings.HasUsers)
            {
                var authInfo = inputHeaders.Get("Authorization");
                
                if(!String.IsNullOrEmpty(authInfo))
                {
                    authInfo = authInfo.TrimEnd();
                    var authInfoParts = authInfo.Split(new []{' '}, StringSplitOptions.RemoveEmptyEntries);
                    if (authInfoParts.Length != 2)
                    {
                        return AnupuReason.InvalidRequest;
                    }
                    
                    if (!string.Equals("Basic", authInfoParts[0].Trim(), StringComparison.InvariantCulture))
                    {
                        return AnupuReason.InvalidRequest;
                    }
                    
                    
                    authInfo = authInfo.Substring(authInfo.LastIndexOf(' '));
                    
                    try
                    {
                        var usernameAndPassword = Base64Decode(authInfo);
                        var parts = usernameAndPassword.Split(':');
                        if(parts.Length != 2)
                        {
                            return AnupuReason.InvalidRequest;
                        }
                        
                        return this.settings.AuthenticateUser(parts[0].Trim(), parts[1].Trim());
                    }
                    catch(Exception e)
                    {
                        throw new InvalidOperationException(authInfo, e);
                    }
//					return AnupuReason.Success;
                }
                
                return AnupuReason.UnknownUser;
            }
            
            return AnupuReason.Success;
        }
    }
}
