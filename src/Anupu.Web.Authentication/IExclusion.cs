﻿/* Created By tyoung On 1/16/2010 11:15 PM */
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Configuration;

namespace Anupu.Web.Authentication
{
    /// <summary>
    /// The code that does an exculsion.
    /// </summary>
    public interface IExclusion
    {
        /// <summary>
        /// Gets the domain.
        /// </summary>
        string Domain { get; }
    }
}

