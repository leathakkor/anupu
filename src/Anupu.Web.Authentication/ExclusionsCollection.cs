﻿/* Created By tyoung On 1/16/2010 11:14 PM */
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Anupu.Web.Authentication
{
    /// <summary>
    /// A collection of ExclusionElement(s).
    /// </summary>
    public sealed class ExclusionsCollection : AnupuConfigurationCollectionBase<ExclusionElement, IExclusion>, IExclusionCollection
    {
        /// <summary>
        /// Adds a new Exclusion.
        /// </summary>
        /// <param name="key">
        /// The exclusions domain.
        /// </param>
        /// <param name="domain">
        /// The exclusions domain.
        /// </param>
        public void Add(string key, string domain)
        {
            this.Add(new ExclusionElement
                     {
                         Key = key,
                         Domain = domain
                     });
            
        }
        
        /// <summary>
        /// This method returns the correct item by domain.
        /// </summary>
        /// <param name="domain">The domain to search for.</param>
        /// <returns>The found Exclusion.</returns>
        public IExclusion FindByDomain(string domain)
        {
            return ((IEnumerable<IExclusion>)this)
                .OfType<IExclusion>()
                .FirstOrDefault(x => string.Equals(x.Domain, domain, StringComparison.InvariantCultureIgnoreCase));
        }
        
        /// <summary>
        /// Gets the Name of Elements of the collection.
        /// </summary>
        protected override string ElementName
        {
            get { return "exclusion"; }
        }
        
        /// <summary>
        /// Gets the element's key.
        /// </summary>
        /// <param name="item">The item for which the key will be gotten.</param>
        /// <returns>The Found key.</returns>
        protected override string GetKey(ExclusionElement item)
        {
            return item.Key;
        }
    }
}

