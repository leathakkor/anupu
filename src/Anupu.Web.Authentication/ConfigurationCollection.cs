﻿using System.Collections.Generic;

namespace Anupu.Web.Authentication
{
    
using System;

    /// <summary>
    /// Utility methods for the <see cref="IConfigurationCollection{T}" /> class.
    /// </summary>
    internal static class ConfigurationCollection
    {
        /// <summary>
        /// This method converts a ConfigurationCollection to a List.
        /// </summary>
        /// <param name="coll">The collection.</param>
        /// <returns>The new list.</returns>
        public static IEnumerable<T> CreateCopy<T>(this IConfigurationCollection<T> coll)
        {
            List<T> result = new List<T>();
            
            for (int i = 0; i < coll.Count; i++)
            {
                var item = coll.FindByIndex(i);
                result.Add(item);
            }
            
            return result;
        }
    }
}
