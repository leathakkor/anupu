﻿/* Created By tyoung On 1/16/2010 11:14 PM */
using System;
using System.Configuration;


namespace Anupu.Web.Authentication
{
    /// <summary>
    /// A collection of UsersElement(s).
    /// </summary>
    public sealed class UsersCollection : AnupuConfigurationCollectionBase<UsersElement, IAnupuUser>, IUsersCollection
    {
        /// <summary>
        /// Add a new User.
        /// </summary>
        /// <param name="userName">
        /// The user's authentication name to add.
        /// </param>
        /// <param name="password">
        /// The user's authentication password
        /// </param>
        /// <param name="maxInvalidAttempts">
        /// The User's maximum number of invalid passwords.
        /// </param>
        public void Add(String userName, string password, int maxInvalidAttempts)
        {
            this.Add(new UsersElement
                     {
                         Name = userName,
                         Password = password,
                         MaxInvalidAttempts = maxInvalidAttempts
                     });
            
        }
        
        /// <summary>
        /// Gets the element's key.
        /// </summary>
        /// <param name="item">The item for which the key will be gotten.</param>
        /// <returns>The Found key.</returns>
        protected override string GetKey(UsersElement item)
        {
            return item.Name;
        }
        
        /// <summary>
        /// Gets the Name of Elements of the collection.
        /// </summary>
        protected override string ElementName
        {
            get { return "user"; }
        }
        
    }
}
