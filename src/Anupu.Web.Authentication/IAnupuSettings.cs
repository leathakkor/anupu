﻿/* Created By tyoung On 1/16/2010 11:15 PM */
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Configuration;

namespace Anupu.Web.Authentication
{
	/// <summary>
	/// The contract that coordinates the backend data store, to the User Interface.
	/// </summary>
	public interface IAnupuSettings
	{
		/// <summary>
		/// A method To findUsers
		/// </summary>
		IEnumerable<IAnupuUser> Users{ get; }
		
		/// <summary>
		/// A method To findUsers
		/// </summary>
		IEnumerable<IExclusion> Exclusions{ get; }
		
		/// <summary>
		/// The Realm associated with this Instance.
		/// </summary>
		string ZoneName { get; set; }
		
		/// <summary>
		/// This property is used to determine if the backend data stre is marked as readonly.
		/// </summary>
		bool IsInMemory {get;set;}
		
		/// <summary>
		/// If the Collection has Exclusions.
		/// </summary>
		bool HasExclusions { get; }
		
		/// <summary>
		/// If the Collection has Users.
		/// </summary>
		bool HasUsers { get; }
		
		/// <summary>
		/// The text to display when a user is locked out of the site.
		/// </summary>
		String LockedOutText { get; }
		
		/// <summary>
		/// The text to display when a user is locked out of the site.
		/// </summary>
		String AuthenticationInstructions { get; }
		
		/// <summary>
		/// Retrieve a user, by UserName
		/// </summary>
		IAnupuUser FindUserByName(String userName);
		
		/// <summary>
		/// Add a new User.
		/// </summary>
		/// <param name="userName">
		/// The user's authentication name to add.
		/// </param>
		/// <param name="password">
		/// The user's authentication password
		/// </param>
		/// <param name="maxInvalidAttempts">
		/// The User's maximum number of invalid passwords.
		/// </param>
		void AddUser(String userName, string password, int maxInvalidAttempts);
		
		/// <summary>
		/// Remove a user
		/// </summary>
		/// <param name="userName">
		/// The name of the user to remove.
		/// </param>
		void RemoveUser(String userName);
		
		/// <summary>
		/// Adds a new Exclusion.
		/// </summary>
		/// <param name="key">
		/// The exclusions domain.
		/// </param>
		/// <param name="domain">
		/// The exclusions domain.
		/// </param>
		 void AddExclusion(string key, string domain);
		
		/// <summary>
		/// Save any outstanding changes to the data store.
		/// </summary>
		void Save();
		
//		/// <summary>
//		/// Try authenticating a user.
//		/// </summary>
//		/// <param name="username">
//		/// Try authenticating with this user's name
//		/// </param>
//		/// <param name="pasword">
//		/// Try authenticating with this user's password
//		/// </param>
//		/// <returns>
//		/// Returns a boolean that is true if the authentication succeded.
//		/// </returns>
//		AnupuReason AuthenticateUser(string username, string pasword);
	    
		
		
		/// <summary>
		/// Retrieve an exclusion, by domain
		/// </summary>
        IExclusion FindExclusionByDomain(string domain);
	}
}

