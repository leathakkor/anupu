﻿/* Created By tyoung On 1/16/2010 11:42 PM */


namespace Anupu.Web.Authentication.Tests
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Reflection;

    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class DomainExclusionTests : AnupuTestBase
    {
        [Test]
        public void ShouldFindExcludedDomains()
        {
            Settings.HasExclusions.Should().BeFalse();
            Settings.AddExclusion("domain:geo.com", "www.geonetric.com");
            Settings.Save();
            Settings.HasExclusions.Should().BeTrue();
            
        }
        
        [Test]
        public void ShouldNotBeAbleToAddDuplicates()
        {   
            Settings.HasExclusions.Should().BeFalse();
            Settings.AddExclusion("domain:geo1.com", "www.geonetriC.com");
            new Action(() => Settings.AddExclusion("domain:geo1.com", "www.geonetr1c.com")).ShouldThrow<ArgumentException>();
            Settings.AddExclusion("domain:geo2.com", "www.geonetr1c.com");
            Settings.Save();
            Settings.HasExclusions.Should().BeTrue();
            
        }
    }
}
