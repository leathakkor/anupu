﻿/* Created By tyoung On 1/16/2010 11:42 PM */
using System;
using System.Configuration;
using FluentAssertions;
using NUnit.Framework;

namespace Anupu.Web.Authentication.Tests
{
	/// <summary>
	/// Description of ConfigurationTests.
	/// </summary>
	[TestFixture]
	public class UserValidationTests : AnupuTestBase
	{
		[Test]
		public void TestLockedOuttedNess()
		{
			Settings.AddUser("syoungblut","poopFace!", 5);
			Settings.Save();
			
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.LockedOut);
		}
		
		[Test]
		public void ShouldReturnNoUserFound()
		{
			Settings.AddUser("syoungblut","poopFace!", 5);
			Settings.Save();
			
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AnupuReason.UnknownUser);
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AnupuReason.UnknownUser);
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AnupuReason.UnknownUser);
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AnupuReason.UnknownUser);
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AnupuReason.UnknownUser);
			Settings.AuthenticateUser("syouNgblut2", "poopfac3!").Should().Be(AnupuReason.UnknownUser);
		}
		
		[Test]
		public void TestLockedOuttedNessResets()
		{
			Settings.AddUser("syoungblut","poopFace!", 5);
			Settings.Save();
			
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);

			Settings.AuthenticateUser("syouNgblut", "poopface!").Should().Be(AnupuReason.Success);
			
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.BadPassword);
			Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.LockedOut);
		}
		
		[Test]
		public void TestAddSameUser2Times()
		{
			Settings.AddUser("Geonetric", "geonet123", 5);
			Settings.Save();
			Settings.AddUser("Geonetric", "geonet123", 5);
			Settings.Save();
		}

		[Test]
		public void ShouldAuthenticateWhenNoUsersSetUpNoCriteria()
		{
		    Settings.Deactivate();
		    Settings.AuthenticateUser("syouNgblut", "poopfac3!").Should().Be(AnupuReason.Success);
		    Settings.AuthenticateUser("", "").Should().Be(AnupuReason.Success);
		    Settings.AuthenticateUser("", "").Should().Be(AnupuReason.Success);
		    Settings.AuthenticateUser(null, "").Should().Be(AnupuReason.Success);
		    Settings.AuthenticateUser("", null).Should().Be(AnupuReason.Success);
		    Settings.AuthenticateUser(null, null).Should().Be(AnupuReason.Success);
		}
	}
}
