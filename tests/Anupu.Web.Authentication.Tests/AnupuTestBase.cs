﻿
using System;
using System.Configuration;
using System.Linq;

using NUnit.Framework;

namespace Anupu.Web.Authentication.Tests
{
    public abstract class AnupuTestBase : IPathMapper
    {
        private int counter = 0;
        
        private IAnupuSettings settings;
        
        protected IAnupuSettings Settings
        {
            get { return this.settings; }
        }
        
        protected IAnupuSettings GetUsersSection(String name)
        {
            return Anupu.CreateUserSettings(this.GetType().Name + "." + name, this);
        }
        
        [SetUp]
        public void TestSetup()
        {
            counter++;
            this.settings = this.GetUsersSection("section" + Environment.TickCount);
        }
    	
		string IPathMapper.MapPath(string pathToMap)
		{
		    string rez = new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath;
		    
		    return rez;
		}
    }
}
