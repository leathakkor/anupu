﻿<%@ Page
	Language           = "C#"
	AutoEventWireup    = "false"
	ValidateRequest    = "false"
	EnableSessionState = "false"
%>
<%@Import NameSpace="Anupu.Web.Authentication" %>
<script runat="server">
	protected AuthenticationProcessor Processor { get { return global::Anupu.Web.Authentication.HttpModule.Processor; } }

	protected void Bind()
	{
		this.currentUsers.DataSource = Processor.Users;
		this.currentUsers.DataBind();
	}
	protected override void OnInit(System.EventArgs ea)
	{
		base.OnInit(ea);
		Bind();
	}
	
	protected void HandleManageClick(System.Object sender, System.EventArgs ea)
	{
		System.Web.UI.WebControls.Button button  = sender as System.Web.UI.WebControls.Button;
		if(button.CommandName == "edit")
		{
			IAnupuUser user = Processor.Users[button.CommandArgument];
			userNameTextBox.Text = user.Name;
			passwordTextBox.Text = user.Password;
			maxInvalidTextBox.Text = user.MaxInvalidAttempts.ToString();
		}
		if(button.CommandName == "remove")
		{
			Processor.Users.Remove(button.CommandArgument);
			Bind();
		}
	}
	
	protected void HandleAddUser(System.Object sender, System.EventArgs ea)
	{
		Processor.Users.Add(
			userNameTextBox.Text,
			passwordTextBox.Text,
			System.Int32.Parse(maxInvalidTextBox.Text)
			);
		Bind();
	}
	
	
	
	
	
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Edit Auth Info</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
		<meta http-equiv="PRAGMA" content="NO-CACHE" />
		
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<fieldset>
				<legend>Add User</legend>
				<div>
					<asp:Label associatedControlId="userNameTextBox" runat="server" Text="User Name" />
					<asp:Textbox runat="server" id="userNameTextBox" />
					<asp:RequiredFieldValidator runat="server" display="dynamic" ControlToValidate="userNameTextBox" Text="X" />
				</div>
				<div>
					<asp:Label associatedControlId="passwordTextBox" runat="server" Text="Password" />
					<asp:Textbox runat="server" id="passwordTextBox" />
					<asp:RequiredFieldValidator runat="server" display="dynamic" ControlToValidate="passwordTextBox" Text="X" />
				</div>
				<div>
					<asp:Label associatedControlId="maxInvalidTextBox" runat="server" Text="Max Invalid Password Attemps" />
					<asp:Textbox runat="server" id="maxInvalidTextBox" />
					<asp:RequiredFieldValidator runat="server" display="dynamic" ControlToValidate="maxInvalidTextBox" Text="X" />
					<asp:RangeValidator runat="server" display="dynamic" ControlToValidate="maxInvalidTextBox" Text="X" MinimumValue="0" MaximumValue="255" Type="Integer" />
				</div>
				<div>
					<asp:Button runat="server" onClick="HandleAddUser" ID="SaveButton" Text="Save" />
				</div>
			</fieldset>
			<!--OnItemDataBound="HandleItemBound" -->
			<table>
				<thead>
					<td>
						User Name
					</td>
					<td>
						Password
					</td>
					<td>
						Max Invalid Attemps
					</td>
					<td>
						Current Invalid Attemps
					</td>
					<td>
						Manage
					</td>
					
				</thead>
			<asp:Repeater runat="server" id="currentUsers" >
				<ItemTemplate>
					<tr>
						<td>
							<asp:literal runat="server" id="username" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
						</td>
						<td>
							<asp:literal runat="server" id="password" Text='<%# DataBinder.Eval(Container.DataItem, "Password") %>' />
						</td>
						<td>
							<asp:literal runat="server" id="maxInvalid" Text='<%# DataBinder.Eval(Container.DataItem, "MaxInvalidAttempts") %>' />
						</td>
						<td>
							<asp:literal runat="server" id="currentInvalid" Text='<%# DataBinder.Eval(Container.DataItem, "CurrentInvalidAttempts") %>' />
						</td>
						<td>
							<asp:Button runat="server" Text="Edit" OnClick="HandleManageClick" CommandName="edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
							<asp:Button runat="server" Text="Remove" OnClick="HandleManageClick" CommandName="remove" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
						</td>
				</ItemTemplate>
			</asp:Repeater>
			</table>
		</form>
	</body>
</html>
