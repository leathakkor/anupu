import sys, os
sys.path.append(r"..\Archimedes")
sys.path.append(r"Archimedes")


from logger import *
from task import Task
from msbuild import MSBuild
from publish import Publish
from nunit import NUnit
from clean import MSClean
from msbuildutil import SLN
from packager import DeployArchimedes
from packager import ZipIt
from packager import CreateBuildScript
from license import MITLicense


logger = DiskLogger(False)


params = SLN.GetCommonParams({"solution":"Anupu.sln"})

dependencies = [
                    CreateBuildScript(logger),
                    MITLicense(logger),
                    MSBuild(logger),
                    NUnit(logger),
                    Publish(logger),
                    DeployArchimedes(logger),
                    ZipIt(logger)
                ]
modules = Task(logger, params, dependencies)
if not modules.invoke():
    print("Failed")
